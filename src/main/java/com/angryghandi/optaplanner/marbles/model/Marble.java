package com.angryghandi.optaplanner.marbles.model;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.io.Serializable;

/**
 * Planning Entity
 * <p>
 * Created by angryghandi on 6/26/17.
 */
@PlanningEntity
public class Marble implements Serializable {

    private static final long serialVersionUID = 2664708908833528303L;

    public Marble() {
    }

    public Marble(final Long id, final Color color) {
        this();
        this.id = id;
        this.color = color;
    }

    private Long id;

    private Color color;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @PlanningVariable(valueRangeProviderRefs = "colorRange")
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
