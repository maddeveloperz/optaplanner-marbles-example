package com.angryghandi.optaplanner.marbles.model;

import com.angryghandi.optaplanner.marbles.util.MarbleUtil;
import com.google.common.collect.Lists;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Planning Solution
 * <p>
 * Created by angryghandi on 6/26/17.
 */
@PlanningSolution
public class MarbleSolution implements Solution<HardSoftScore>, Serializable {

    private static final long serialVersionUID = 2744632091885832840L;

    private List<Marble> marbles;

    private HardSoftScore score;

    public MarbleSolution() {
        initializeMarbleCollection();
    }

    private void initializeMarbleCollection() {
        marbles = MarbleUtil.createRandomMarbleList();
    }

    @SuppressWarnings("unused")
    @ValueRangeProvider(id = "colorRange")
    public List<Color> colors() {
        return Lists.newArrayList(Color.values());
    }

    @PlanningEntityCollectionProperty
    public List<Marble> getMarbles() {
        return marbles;
    }

    public void setMarbles(List<Marble> marbles) {
        this.marbles = marbles;
    }

    @Override
    public HardSoftScore getScore() {
        return score;
    }

    @Override
    public void setScore(HardSoftScore score) {
        this.score = score;
    }

    @Override
    public Collection<?> getProblemFacts() {
        // collection is managed by OptaPlanner during Solution cloning...
        return new ArrayList<>();
    }
}
