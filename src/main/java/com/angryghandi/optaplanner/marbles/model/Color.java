package com.angryghandi.optaplanner.marbles.model;

/**
 * Marble Colors
 * <p>
 * Created by angryghandi on 6/26/17.
 */
public enum Color {

    RED, GREEN, BLUE, YELLOW;

    @Override
    public String toString() {
        return name().substring(0, 1);
    }

}
