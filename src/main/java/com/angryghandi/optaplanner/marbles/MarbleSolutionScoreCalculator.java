package com.angryghandi.optaplanner.marbles;

import com.angryghandi.optaplanner.marbles.model.Color;
import com.angryghandi.optaplanner.marbles.model.Marble;
import com.angryghandi.optaplanner.marbles.model.MarbleSolution;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

/**
 * Marble Score Calculator
 * <p>
 * Created by angryghandi on 6/26/17.
 */
public class MarbleSolutionScoreCalculator implements EasyScoreCalculator<MarbleSolution> {

    /*
     * Discourage sequences of marbles with the same color by soft constraint
     */
    public Score calculateScore(MarbleSolution solution) {
        HardSoftScore score = HardSoftScore.valueOf(0, 0);

        Color lastColor = null;
        for (Marble marble : solution.getMarbles()) {
            if (marble.getColor() == lastColor) {
                score = score.subtract(HardSoftScore.valueOf(0, 1));
            }
            lastColor = marble.getColor();
        }

        return score;
    }

}
