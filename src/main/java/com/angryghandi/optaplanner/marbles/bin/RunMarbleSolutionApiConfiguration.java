package com.angryghandi.optaplanner.marbles.bin;

import com.angryghandi.optaplanner.marbles.MarbleSolutionScoreCalculator;
import com.angryghandi.optaplanner.marbles.model.Marble;
import com.angryghandi.optaplanner.marbles.model.MarbleSolution;
import com.angryghandi.optaplanner.marbles.util.MarbleUtil;
import com.google.common.collect.Lists;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.config.localsearch.LocalSearchPhaseConfig;
import org.optaplanner.core.config.localsearch.decider.acceptor.AcceptorConfig;
import org.optaplanner.core.config.localsearch.decider.forager.LocalSearchForagerConfig;
import org.optaplanner.core.config.score.definition.ScoreDefinitionType;
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;
import org.optaplanner.core.config.solver.SolverConfig;
import org.optaplanner.core.config.solver.termination.TerminationConfig;

import java.util.Collections;

/**
 * Demonstrate configuring OptaPlanner via Java API
 * <p>
 * Created by angryghandi on 6/26/17.
 */
public class RunMarbleSolutionApiConfiguration {

    public static void main(String[] args) {

        /*
         * Build solver config
         * - identify the solution class and planning entity
         */
        SolverConfig solverConfig = new SolverConfig();
        solverConfig.setSolutionClass(MarbleSolution.class);
        solverConfig.setEntityClassList(Collections.singletonList(Marble.class));

        /*
         * Build score director config
         * - use hard and soft constraints
         * - use a custom java class to calculate the score
         */
        ScoreDirectorFactoryConfig scoreDirectorFactoryConfig = new ScoreDirectorFactoryConfig();
        scoreDirectorFactoryConfig.setScoreDefinitionType(ScoreDefinitionType.HARD_SOFT);
        scoreDirectorFactoryConfig.setEasyScoreCalculatorClass(MarbleSolutionScoreCalculator.class);

        /*
         * Add the score director config to the solver config
         */
        solverConfig.setScoreDirectorFactoryConfig(scoreDirectorFactoryConfig);

        /*
         * Build termination config
         * - terminate after 5 seconds...
         */
        TerminationConfig terminationConfig = new TerminationConfig();
        terminationConfig.setSecondsSpentLimit(5L);

        /*
         * Add termination config to solver config
         */
        solverConfig.setTerminationConfig(terminationConfig);

        /*
         * Perform Local Search: start from an initial solution and evolves that single solution into a
         * mostly better and better solution...
         */
        LocalSearchPhaseConfig localSearchPhaseConfig = new LocalSearchPhaseConfig();

        /*
         * Tabu Search takes steps it creates one or more tabu's. For a number of steps, it does not
         * accept a move if that move breaks tabu. That number of steps is the tabu size.
         * See: https://en.wikipedia.org/wiki/Tabu_search
         */
        AcceptorConfig acceptorConfig = new AcceptorConfig();
        acceptorConfig.setEntityTabuSize(7);

        /*
         * A Forager gathers all accepted moves and picks the move which is the next step.
         */
        LocalSearchForagerConfig localSearchForagerConfig = new LocalSearchForagerConfig();

        /*
         * how many accepted moves should be evaluated during each step
         */
        localSearchForagerConfig.setAcceptedCountLimit(1000);

        /*
         * Add acceptor and forager to local search phase config
         */
        localSearchPhaseConfig.setAcceptorConfig(acceptorConfig);
        localSearchPhaseConfig.setForagerConfig(localSearchForagerConfig);

        solverConfig.setPhaseConfigList(Lists.newArrayList(localSearchPhaseConfig));

        /*
         * Build the solver from the established configuration
         */
        Solver solver = solverConfig.buildSolver();

        /*
         * Create planning solution
         */
        MarbleSolution marbleSolution = new MarbleSolution();

        /*
         * Print the initial randomized marble collection
         */
        System.out.println(String.format("Initial Marble Collection (duplicate colors = %s)",
                MarbleUtil.duplicateColors(marbleSolution.getMarbles())));
        MarbleUtil.printMarbles(marbleSolution.getMarbles());

        /*
         * Solve
         */
        solver.solve(marbleSolution);

        /*
         * Get the best solution
         */
        MarbleSolution bestMarbleSolution = (MarbleSolution) solver.getBestSolution();


        /*
         * Print the solution
         */
        System.out.println(String.format("Solved Marble Collection (duplicate colors = %s)",
                MarbleUtil.duplicateColors(bestMarbleSolution.getMarbles())));
        MarbleUtil.printMarbles(bestMarbleSolution.getMarbles());
    }

}
