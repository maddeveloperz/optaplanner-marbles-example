package com.angryghandi.optaplanner.marbles.bin;

import com.angryghandi.optaplanner.marbles.model.MarbleSolution;
import com.angryghandi.optaplanner.marbles.util.MarbleUtil;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

/**
 * Demonstrate configuring OptaPlanner Solution via XML
 * <p>
 * Created by angryghandi on 6/26/17.
 */
public class RunMarbleSolutionXmlConfiguration {

    /*
     * Solver Config XML
     */
    private static final String SOLVER_CONFIG_XML = "com/angryghandi/optaplanner/marbles/marbleSolverConfig.xml";

    public static void main(String[] args) {
        /*
         * Create solver from XML
         */
        Solver solver = SolverFactory.createFromXmlResource(SOLVER_CONFIG_XML).buildSolver();

        /*
         * Create planning solution
         */
        MarbleSolution marbleSolution = new MarbleSolution();

        /*
         * Print the initial randomized marble collection
         */
        System.out.println("Initial Marble Collection (random colors)");
        MarbleUtil.printMarbles(marbleSolution.getMarbles());

        /*
         * Solve
         */
        solver.solve(marbleSolution);

        /*
         * Get the best solution
         */
        MarbleSolution bestMarbleSolution = (MarbleSolution) solver.getBestSolution();

        /*
         * Print the solution
         */
        System.out.println("Solved Marble Collection (favored colors)");
        MarbleUtil.printMarbles(bestMarbleSolution.getMarbles());
    }

}
