package com.angryghandi.optaplanner.marbles.util;

import com.angryghandi.optaplanner.marbles.model.Color;
import com.angryghandi.optaplanner.marbles.model.Marble;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Utility to create and display {@link Marble} collections
 * <p>
 * Created by angryghandi on 6/27/17.
 */
public class MarbleUtil {

    public static final int DEFAULT_SIZE = 100;

    public static List<Marble> createRandomMarbleList() {
        return createRandomMarbleList(DEFAULT_SIZE);
    }

    public static List<Marble> createRandomMarbleList(int size) {
        List<Marble> marbles = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Marble marble = new Marble();
            marble.setId((long) i);
            marble.setColor(randomColor());
            marbles.add(marble);
        }
        return marbles;
    }

    public static Color randomColor() {
        Color[] colors = Color.values();

        return colors[new Random().nextInt(Color.values().length)];
    }

    public static void printMarbles(final List<Marble> marbles) {
        System.out.println();
        System.out.println(marblesToString(marbles));
        System.out.println();
    }

    public static String marblesToString(final List<Marble> marbles) {
        StringBuilder sb = new StringBuilder();
        if (null == marbles) {
            return sb.toString();
        }

        int counter = 0;
        for (Marble marble : marbles) {
            counter++;

            sb.append(marble.getColor());
            sb.append(" ");

            if (counter == 10) {
                sb.append("\n");
                counter = 0;

            }
        }
        return sb.toString();
    }

    public static boolean duplicateColors(final List<Marble> marbles) {
        if (null == marbles || marbles.isEmpty()) {
            return false;
        }
        Color last = null;
        for (Marble marble : marbles) {
            if (marble.getColor() == last) {
                return true;
            }
            last = marble.getColor();
        }
        return false;
    }
}
