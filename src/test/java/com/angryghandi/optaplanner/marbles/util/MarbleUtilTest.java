package com.angryghandi.optaplanner.marbles.util;

import com.angryghandi.optaplanner.marbles.model.Color;
import com.angryghandi.optaplanner.marbles.model.Marble;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit Tests for {@link MarbleUtil}
 * <p>
 * Created by angryghandi on 6/27/17.
 */
public class MarbleUtilTest {

    @Test
    public void marblesToString_with_null() {
        assertEquals("", MarbleUtil.marblesToString(null));
    }

    @Test
    public void marblesToString_with_emptyList() {
        assertEquals("", MarbleUtil.marblesToString(Collections.emptyList()));
    }

    @Test
    public void marblesToString_with_one_marble() {
        List<Marble> marbles = createMarbles(Color.RED); //Lists.newArrayList(new Marble(1L, Color.RED));
        assertEquals("R ", MarbleUtil.marblesToString(marbles));
    }

    @Test
    public void marblesToString_with_twelve_marbles() {
        List<Marble> marbles = createMarbles(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        assertEquals(12, marbles.size());
        assertEquals("R G B Y R G B Y R G \nB Y ", MarbleUtil.marblesToString(marbles));
    }

    @Test
    public void createRandomMarbleList() {
        List<Marble> marbles = MarbleUtil.createRandomMarbleList();
        assertNotNull(marbles);
        assertEquals(MarbleUtil.DEFAULT_SIZE, marbles.size());

        for (Marble marble : marbles) {
            assertNotNull(marble.getId());
            assertNotNull(marble.getColor());
        }
    }

    @Test
    public void createRandomMarbleList_with_size() {
        int size = 5;
        List<Marble> marbles = MarbleUtil.createRandomMarbleList(size);
        assertNotNull(marbles);
        assertEquals(size, marbles.size());

        for (Marble marble : marbles) {
            assertNotNull(marble.getId());
            assertNotNull(marble.getColor());
        }
    }

    @Test
    public void randomColor() {
        // should never be null...
        Color color = MarbleUtil.randomColor();
        assertNotNull(color);
    }

    @Test
    public void duplicateColors_with_duplicates() {
        List<Marble> marbles = createMarbles(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                Color.RED, Color.GREEN, Color.YELLOW, Color.YELLOW);
        assertEquals(12, marbles.size());
        assertTrue(MarbleUtil.duplicateColors(marbles));
    }

    @Test
    public void duplicateColors_no_duplicates() {
        List<Marble> marbles = createMarbles(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        assertEquals(12, marbles.size());
        assertFalse(MarbleUtil.duplicateColors(marbles));
    }

    private List<Marble> createMarbles(Color... colors) {
        if (null == colors || colors.length == 0) {
            return Collections.emptyList();
        }
        List<Marble> marbles = new ArrayList<>();
        for (int i = 0; i < colors.length; i++) {
            marbles.add(new Marble((long) i, colors[i]));
        }
        return marbles;
    }

}
