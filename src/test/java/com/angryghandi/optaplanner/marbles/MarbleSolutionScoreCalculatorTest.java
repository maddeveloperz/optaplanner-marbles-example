package com.angryghandi.optaplanner.marbles;

import com.angryghandi.optaplanner.marbles.model.Color;
import com.angryghandi.optaplanner.marbles.model.Marble;
import com.angryghandi.optaplanner.marbles.model.MarbleSolution;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link MarbleSolutionScoreCalculator}
 * <p>
 * Created by angryghandi on 6/27/17.
 */
public class MarbleSolutionScoreCalculatorTest {

    private MarbleSolutionScoreCalculator scoreCalculator;

    @Before
    public void before() {
        scoreCalculator = new MarbleSolutionScoreCalculator();
    }

    @Test
    public void calculateScore_four_consecutive_marbles_same_color() {
        List<Marble> marbles = Lists.newArrayList(new Marble(1L, Color.GREEN), new Marble(2L, Color.GREEN),
                new Marble(3L, Color.GREEN), new Marble(4L, Color.GREEN));
        MarbleSolution marbleSolution = new MarbleSolution();
        marbleSolution.setMarbles(marbles);

        HardSoftScore score = (HardSoftScore) scoreCalculator.calculateScore(marbleSolution);
        assertEquals(0, score.getHardScore());
        assertEquals(-3, score.getSoftScore());
    }

    @Test
    public void calculateScore_consecutive_marbles_same_color() {
        List<Marble> marbles = Lists.newArrayList(new Marble(1L, Color.GREEN), new Marble(2L, Color.GREEN));
        MarbleSolution marbleSolution = new MarbleSolution();
        marbleSolution.setMarbles(marbles);

        HardSoftScore score = (HardSoftScore) scoreCalculator.calculateScore(marbleSolution);
        assertEquals(0, score.getHardScore());
        assertEquals(-1, score.getSoftScore());
    }

    @Test
    public void calculateScore_consecutive_marbles_different_color() {
        List<Marble> marbles = Lists.newArrayList(new Marble(1L, Color.GREEN), new Marble(2L, Color.RED));
        MarbleSolution marbleSolution = new MarbleSolution();
        marbleSolution.setMarbles(marbles);

        HardSoftScore score = (HardSoftScore) scoreCalculator.calculateScore(marbleSolution);
        assertEquals(0, score.getHardScore());
        assertEquals(0, score.getSoftScore());
    }
}
